In [3]: import numpy as np
   ...: import matplotlib.pyplot as plt
   ...: from mpl_toolkits.mplot3d import Axes3D
   ...: 
   ...: X,Y = np.meshgrid(np.linspace(-5, 5, 100), np.linspace(-5,5,100))
   ...: Z = np.sin(np.sqrt(X**2 + Y**2))
   ...: 
   ...: fig = plt.figure()
   ...: ax = fig.add_subplot(111, projection="3d")
   ...: ax.plot_surface(X,Y,Z)
   ...: plt.show()

