import numpy as np
   ...: import matplotlib.pyplot as plt
   ...: from mpl_toolkits.mplot3d import Axes3D
   ...: 
   ...: X,Y = np.meshgrid(np.linspace(-np.pi/2, np.pi/2, 100), np.linspace(-np.pi/2,np.pi/2,100))
   ...: Z = np.sin(3*X) + np.cos(Y/2) + np.cos(X+Y)
   ...: 
   ...: fig = plt.figure()
   ...: ax = fig.add_subplot(111, projection="3d")
   ...: ax.plot_surface(X,Y,Z, cmap="viridis")
   ...: plt.show()
