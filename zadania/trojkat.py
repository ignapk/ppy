from random import random

predict = ["***","**_","*_*","*__","_**","_*_","__*","___"]
rule = "01011010"
k = 30
n = 51
s = "_" * 25 + "*" + "_" * 25
r = {k:v for k,v in zip(predict,["_" if i == '0' else "*" for i in rule])}

print("regula: " + rule)
print("liczba krokow: " + str(k))
print("")
print(s)
for i in range(k):
    s = s[-1] + s + s[0]
    s = "".join(r[s[i-1:i+2]] for i in range(1,n+1))
    print(s)
