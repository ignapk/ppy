import itertools
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np

n = 100
board = np.random.randint(2, size=(n, n))
colors = np.repeat(board[:, :, None], 3, axis=2).astype(float)
fig, axs = plt.subplots()
mats = axs.matshow(colors)

def active_neighbours(board, i, j):
    ret = board[i-1:i+2,j-1:j+2].sum()
    ret = ret - board[i,j]
    return ret

def update(_):
  board_copy = board.copy()
  for i, j in itertools.product(range(n), repeat=2):
      sasiady = active_neighbours(board_copy, i, j)
      if board_copy[i,j] == 1:
          if sasiady in (2,3):
              board[i,j] = 1
              colors[i,j] = colors[i,j] - 0.2
          else:
              board[i,j] = 0
              colors[i,j] = np.zeros(3)
      elif board_copy[i,j] == 0 and sasiady == 3:
        board[i,j] = 1
        colors[i,j] = np.ones(3)
  mats.set_array(colors)

ani = FuncAnimation(fig, update)
plt.show()
