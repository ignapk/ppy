import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

Re = input()
Im = input()

c = float(Re) + float(Im)*1j
d = 400

X, Y = np.meshgrid(np.linspace(-1.5, 1.5, d), np.linspace(-1.5, 1.5, d))

z = X + Y * 1j
fig = plt.figure(figsize=(9,11))
tmp = np.ones(z.shape)*255
image = plt.imshow(tmp, vmin=0, vmax=255, cmap='summer')

def update(frame):
    global z
    tmp[np.abs(z)>2] = frame
    z = (tmp == 255) * (z ** 2 + c)
    image.set_array(tmp)

ani = animation.FuncAnimation(fig, update, frames=255)
plt.show()
