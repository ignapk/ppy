import numpy as np
import matplotlib.pyplot as plt

n = 50000
x = 0
y = 0
X = [0]
Y = [0]

for iteration in range(n):
    i = np.random.choice([1, 2, 3, 4], p=[0.85, 0.07, 0.07, 0.01])
    if i == 1:
        x, y = 0.85*x + 0.04*y, -0.04*x + 0.85*y + 1.6
    elif i == 2:
        x, y = -0.15*x + 0.28*y, 0.26*x + 0.24*y + 0.44
    elif i == 3:
        x, y = 0.20*x - 0.26*y, 0.23*x + 0.22*y + 1.6
    else:
        x, y = 0, 0.16*y    
    X.append(x)
    Y.append(y)

plt.plot(X, Y, marker=".", linewidth=0, markersize=3, color='purple')
plt.show()
