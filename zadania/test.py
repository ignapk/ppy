import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation
import sys

x = np.arange(-1.5, 1.5, 0.01)
y = np.arange(-1.5, 1.5, 0.01)
x, y = np.meshgrid(x,y)
z = x + 1j*y
s = z.shape

# c = float(sys.argv[1]) + float(sys.argv[2])*1j
c = input()
c = c.split(" ")
c = float(c[0]) + float(c[1])*1j
res = 255*np.ones(s)
fig = plt.figure(figsize=(12,9))
w = plt.imshow(res, vmin = 0,vmax = 255, cmap = 'hot')

def anim(i):
    global z
    res[np.abs(z)>2] = i
    z = (res == 255) * (z ** 2 + c)
    w.set_array(res)

a = FuncAnimation(fig,anim,frames=255,interval=80,repeat=False)
plt.show()
